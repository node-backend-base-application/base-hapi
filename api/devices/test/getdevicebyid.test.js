//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let should = chai.should();

chai.use(chaiHttp);
describe("Devices", () => {
  /*
   * Test the /GET route
   */
  describe("/getDeviceById", () => {
    it("get device by id", (done) => {
      chai
        .request("http://localhost:3000")
        .get("/api/v1/devices/getDeviceById/2")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("device_id");
          done();
        });
    });
    it("calling getDeviceById API without devices id ", (done) => {
      chai
        .request("http://localhost:3000")
        .get("/api/v1/devices/getDeviceById/")
        .end((err, res) => {
          res.should.have.status(200);
          // res.body.should.be.a("object");
          // res.body.length.should.be.eql(1);
          done();
        });
    });
  });
});
