"use strict";

const Joi = require("joi");

module.exports = {
  addDeviceSchema() {
    return Joi.object({
      device_id: Joi.number().required(),
      device_name: Joi.string().required(),
      public_name: Joi.string().required(),
      device_category_id: Joi.number().required(),
      dgca_registered_date: Joi.date().required(),
      status: Joi.string().required(),
      u_id: Joi.number().required(),
    }).label("Devices");
  },
  getDeviceById() {
    return Joi.object({
      device_id: Joi.number().required(),
    }).label("Devices");
  },
};
