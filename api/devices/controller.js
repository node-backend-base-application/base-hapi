const Boom = require("@hapi/boom");

module.exports = {
  async addDevice(req, h) {
    const Devices = req.getDb("ga_enterprise").getModel("devices");
    const {
      device_id,
      device_name,
      public_name,
      device_category_id,
      dgca_registered_date,
      status,
      u_id,
    } = req.payload;

    const payload = {
      device_id: device_id,
      device_name: device_name,
      public_name: public_name,
      device_category_id: device_category_id,
      dgca_registered_date: dgca_registered_date,
      status: status,
      created_date: new Date(),
      created_by: u_id,
      edited_date: new Date(),
      edited_by: u_id,
    };
    try {
      return await Devices.create(payload);
    } catch (error) {
      console.log(error);
    }
  },
  async getDeviceById(req, h) {
    const { device_id } = req.params;
    const Devices = req.getDb("ga_enterprise").getModel("devices");
    try {
      return await Devices.findOne({
        where: { device_id: device_id },
      });
    } catch (error) {}
  },
};
