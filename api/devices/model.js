module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "devices",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      device_id: {
        type: DataTypes.INTEGER,
      },
      device_name: {
        type: DataTypes.STRING,
      },
      public_name: {
        type: DataTypes.STRING,
      },
      device_category_id: {
        type: DataTypes.INTEGER,
      },
      dgca_registered_date: {
        type: DataTypes.DATE,
      },
      status: {
        type: DataTypes.STRING,
      },
      created_date: {
        type: DataTypes.DATE,
      },
      created_by: {
        type: DataTypes.INTEGER,
      },
      edited_date: {
        type: DataTypes.DATE,
      },
      edited_by: {
        type: DataTypes.INTEGER,
      },
    },
    {
      freezeTableName: true,
    }
  );
};
