const Joi = require("joi");
const userController = require("./controller");
const userSchema = require("./schema");
const API_VERSION = require("../../config").apiVersion;

module.exports = [
  {
    path: `/api/${API_VERSION}/user/register`,
    method: "POST",
    config: {
      handler: userController.register,
      description: "register user",
      notes: "register user",
      tags: ["api", "user"], // ADD THIS TAG
      validate: {
        payload: userSchema.createUserSchema(),
      },
    },
  },
  {
    path: `/api/${API_VERSION}/user/login`,
    method: "POST",
    config: {
      handler: userController.login,
    },
  },
  {
    path: `/api/${API_VERSION}/user/createUser`,
    method: "POST",
    config: {
      handler: userController.createUser,
      description: "create user",
      notes: "create user",
      tags: ["api", "user"], // ADD THIS TAG
      validate: {
        payload: userSchema.createUserSchema(),
      },
    },
  },
  {
    path: `/api/${API_VERSION}/user/getAllUsers`,
    method: "GET",
    config: {
      handler: userController.getAllUsers,
    },
  },
  {
    path: `/api/${API_VERSION}/user/getUserById/{u_id}`,
    method: "GET",
    config: {
      handler: userController.getUserById,
      description: "get user user",
      notes: "get user user",
      tags: ["api", "user"], // ADD THIS TAG
      validate: {
        params: userSchema.getUserById(),
      },
    },
  },
];
