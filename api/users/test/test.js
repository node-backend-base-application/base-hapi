//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let should = chai.should();

chai.use(chaiHttp);
describe("Users", () => {
  /*
   * Test the /GET route
   */
  describe("/GET User", () => {
    it("it should GET all the User", (done) => {
      chai
        .request("http://localhost:3000")
        .get("/api/v1/user/getAllUsers")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          // res.body.length.should.be.eql(1);
          done();
        });
    });
  });
});
