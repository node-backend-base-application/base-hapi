"use strict";

const Joi = require("joi");

module.exports = {
  createUserSchema() {
    return Joi.object({
      first_name: Joi.string(),
      last_name: Joi.string(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    }).label("User");
  },
  getUserById() {
    return Joi.object({
      u_id: Joi.number().required(),
    }).label("User");
  },
};
