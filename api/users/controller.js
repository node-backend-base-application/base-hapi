require("dotenv").config();
const Boom = require("@hapi/boom");

module.exports = {
  async createUser(req, h) {
    const Users = req.getDb("ga_enterprise").getModel("users");
    const user = {
      first_name: req.payload.first_name,
      last_name: req.payload.last_name,
      email: req.payload.email,
      password: req.payload.password,
    };

    try {
      const createUser = await Users.create(user);
      return createUser;
    } catch (error) {}
  },

  async getAllUsers(req, h) {
    const Users = req.getDb("ga_enterprise").getModel("users");

    try {
      const allUsersDetails = await Users.findAll();
      return allUsersDetails;
    } catch (error) {}
  },
  register: async function (req, h) {
    const Users = req.getDb("ga_enterprise").getModel("users");

    try {
      // Create a Tutorial
      const { first_name, last_name, email, password } = req.payload;

      if (!(email && password && first_name && last_name)) {
        Boom.badRequest("All input is required");
        // res.status(400).send("All input is required");
      }

      const oldUser = await Users.findOne({ where: { email: email } });

      if (oldUser) {
        Boom.conflict("User Already Exist. Please Login");
      }

      // Create user in our database
      let user = await Users.create({
        first_name,
        last_name,
        email: email.toLowerCase(), // sanitize: convert email to lowercase
        password: password,
      });
      // return new user
      return h.response(user);
    } catch (err) {
      console.log(err);
    }
  },

  login: async function (req, h) {
    const Users = req.getDb("ga_enterprise").getModel("users");

    // Our login logic starts here
    try {
      // Get user input
      const { email, password } = req.payload;

      // Validate user input
      if (!(email && password)) {
        Boom.badRequest("All input is required");
      }
      // Validate if user exist in our database
      let user = await Users.findOne({
        where: { email: email, password: password },
      });

      if (user) {
        return h.response(user);
      }
      // res.status(400).send("Invalid Credentials");
      Boom.badRequest("Invalid Credentials");
    } catch (err) {
      console.log(err);
    }
  },
  async getUserById(req, h) {
    const Users = req.getDb("ga_enterprise").getModel("users");

    const { u_id } = req.params;
    try {
      console.log(u_id);
      let user = await Users.findOne({ where: { id: u_id } });
      return h.response(user);
    } catch (error) {}
  },
};
