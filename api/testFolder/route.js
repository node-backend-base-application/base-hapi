const Joi = require("joi");
const deviceController = require("./controller");
const deviceSchema = require("./schema");
const API_VERSION = require("../../config").apiVersion;

module.exports = [
  {
    path: `/api/${API_VERSION}/test/devices/addDevice`,
    method: "POST",
    config: {
      handler: deviceController.addDevice,
      description: "add device",
      notes: "add device",
      tags: ["api", "device"], // ADD THIS TAG
      validate: {
        payload: deviceSchema.addDeviceSchema(),
      },
    },
  },
  {
    path: `/api/${API_VERSION}/test/devices/getDeviceById/{device_id}`,
    method: "GET",
    config: {
      handler: deviceController.getDeviceById,
      description: "get device By Id",
      notes: "get device By Id",
      tags: ["api", "device"], // ADD THIS TAG
      validate: {
        params: deviceSchema.getDeviceById(),
      },
    },
  },
];
