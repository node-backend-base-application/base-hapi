module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "device_category",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      device_name: {
        type: DataTypes.STRING,
      },
    },
    {
      freezeTableName: true,
    }
  );
};
