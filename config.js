const Pack = require("./package");
const Sequelize = require("sequelize");

module.exports = {
  swaggerOptions: {
    info: {
      title: "GA API Documentation",
      version: Pack.version,
    },
    grouping: "tags",
    reuseDefinitions: false,
  },
  dbDevConfig: {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "ga_enterprise",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
  dbProdConfig: {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "ga_enterprise",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
  sequelizeOptions: (dbConfig) => {
    return [
      {
        name: dbConfig.DB, // identifier
        models: [__dirname + "/api/**/model.js"], // paths/globs to model files
        //ignoredModels: [__dirname + "/server/models/**/*.js"], // OPTIONAL: paths/globs to ignore files
        sequelize: new Sequelize(
          dbConfig.DB,
          dbConfig.USER,
          dbConfig.PASSWORD,
          {
            host: dbConfig.HOST,
            dialect: dbConfig.dialect,
            operatorsAliases: 0,
            define: {
              timestamps: false,
            },
            pool: {
              max: dbConfig.pool.max,
              min: dbConfig.pool.min,
              acquire: dbConfig.pool.acquire,
              idle: dbConfig.pool.idle,
            },
          }
        ), // sequelize instance
        sync: false, // sync models - default false
        forceSync: false, // force sync (drops tables) - default false
      },
    ];
  },
  apiVersion: "v1",
};
