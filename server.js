const Hapi = require("@hapi/hapi");
require("dotenv").config();

const Plugins = require("./plugins");

const server = Hapi.server({
  port: process.env.PORT || 3000,
  host: process.env.HOST || "localhost",
});

(async () => {
  await Plugins(server);
  await server.start();
  console.log("Server running on %s", server.info.uri);
})();

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

module.exports = server;
