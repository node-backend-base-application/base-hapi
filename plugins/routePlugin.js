const glob = require("glob");
const path = require("path");

module.exports = {
  name: "routeplugin",
  version: "1.0.0",
  register: function (server) {
    glob
      .sync("api/**/route.js", {
        root: __dirname,
      })
      .forEach((file) => {
        const route = require(path.join(__dirname, `../${file}`));
        server.route(route);
      });
    server.route({
      method: "GET",
      path: "/",
      handler: function (req, h) {
        return "Welcome to the page";
      },
    });
    server.route({
      method: "GET",
      path: "/testReport/{param*}",
      handler: {
        directory: {
          path: "./mochawesome-report",
          index: ["mochawesome.html", "default.html"],
        },
      },
    });
  },
};
