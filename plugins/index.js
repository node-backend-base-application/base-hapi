const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const HapiSwagger = require("hapi-swagger");
const SequelizeJs = require("hapi-sequelizejs");
var fs = require("fs");
require("dotenv").config();
const RoutePlugin = require("./routePlugin");
const Config = require("./../config");

const dbConfig =
  process.env.NODE_ENV === "production" ? Config.dbProdConfig : Config.dbConfig;

module.exports = async function (server) {
  const plugins = await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: Config.swaggerOptions,
    },
    {
      plugin: SequelizeJs,
      options: Config.sequelizeOptions(dbConfig),
    },
    {
      plugin: RoutePlugin,
    },
    {
      plugin: require("hapi-pino"),
      options: {
        prettyPrint: process.env.NODE_ENV !== "production",
        // Redact Authorization headers, see https://getpino.io/#/docs/redaction
        redact: ["req.headers.authorization"],
        logPayload: true,
        stream: fs.createWriteStream("server.log"),
      },
    },
  ]);
  return plugins;
};
